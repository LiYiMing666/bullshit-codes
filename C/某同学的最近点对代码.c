#include<stdio.h>
#include<math.h>
int x[1000001],y[1000001],s[1000001];
int nth(int *a,int L,int R,int k)
{
	if(L==R)return a[L];
	int mid=(L+R)/2;
	int i;int eq=0;
	int l1=0,l2=0;
	for(i=L;i<=R;i++)
	{
		if(a[i]<a[mid])x[++l1]=a[i];
		else if(a[i]>a[mid])y[++l2]=a[i];
		else eq++;
	}
	if(k<=l1)
	{
		for(i=1;i<=l1;i++)s[i]=x[i];
		return nth(s,1,l1,k);
	}
	else if(k<=l1+eq)return a[mid];
	else
	{
		k-=(l1+eq);
		for(i=1;i<=l2;i++)s[i]=y[i];
		return nth(s,1,l2,k);
	}
 } 
 int cmp(int x,int y)
 {
 	if(x<y)return x;
 	return y;
 }
 int jue(int x,int y)
 {
 	if(x-y>=0)return x-y;
	else return y-x; 
 }
typedef struct{
	int x[1000000];
	int y[1000000];
	int sum;
}point;
point a;
int min(point a)
{
	point b,c;
	int i,j;
	if(a.sum==2)
	{
		return sqrt(pow(a.x[0]-a.x[1],2)+pow(a.y[0]-a.y[1],2)); 
	}
	else
	{
		int midx=nth(a.x,1,a.sum-1,a.sum/2);
		b.sum=0;
		c.sum=0;
		for(i=0;i<a.sum;i++)
		{
			if(a.x[i]<=midx)
			{
				b.x[b.sum++]=a.x[i];
				b.y[b.sum]=a.y[i];
			}
			else
			{
				c.x[c.sum++]=a.x[i];
				c.y[c.sum]=a.y[i];
			}
		}
	}
	int d1=min(b);
	int d2=min(c);
	int dm=cmp(d1,d2);
	int dx,min_cur;
	for(i=0;i<b.sum;i++)
	{
		min_cur=dm+1;
		for(j=0;j<c.sum;j++)
		{
			if(jue(b.x[i],c.x[j])<=dm && jue(b.y[i],c.y[j])<=dm)
			{
				dx=sqrt(pow(b.x[i]-c.x[j],2)+pow(b.y[i]-c.y[j],2)); 
				if(dx<min_cur) min_cur=dx;
			}
		}
	}
	return cmp(dm,min_cur);
	
}
int main()
{
	int n;
	scanf("%d",&n);
	a.sum=0;
	while(n--)
	{
		scanf("%d %d",&a.x[a.sum++],&a.y[a.sum]);
	}
	int d=min(a);
	printf("%d\n",d);
	return 0;
}
